import json
from aiogram import Bot, Dispatcher, types, filters, exceptions, F
import asyncio
from dotenv import load_dotenv
import os
import reply
import managers


load_dotenv()

TOKEN = os.getenv("CRYPTO_BOT_TOKEN")
bot = Bot(token=TOKEN)
dp = Dispatcher()
mng = managers.ScrapingCryptoCurrency()


@dp.message(filters.CommandStart())
async def start(message: types.Message) -> None:
    await message.answer(mng.start_message, reply_markup=reply.start_kb)


### Mono To Crypto ###


@dp.message(filters.or_f(filters.Command("Mono Bank"), (F.text.lower() == "mono bank")))
async def base__mono_to_crypto(message: types.Message) -> None:
    await message.answer(mng.choice_message, reply_markup=reply.mono_to_crypto)


mono_to_crypto = managers.MonoToCrypto()


@dp.message(
    filters.or_f(
        filters.Command("MONO TO BITCOIN"), (F.text.lower() == "mono to bitcoin")
    )
)
async def mono_bitcoin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_bitcoin()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO BITCOIN BEP20"),
        (F.text.lower() == "mono to bitcoin bep20"),
    )
)
async def mono_bitcoin_bep20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_bitcoin_bep20()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO  CASH"), (F.text.lower() == "mono to bitcoin cash")
    )
)
async def mono_bitcoin_cash(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_bitcoin_cash()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO ETHEREUM"), (F.text.lower() == "mono to ethereum")
    )
)
async def mono_ethereum(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_ethereum()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO ETHEREUM BEP20"),
        (F.text.lower() == "mono to ethereum bep20"),
    )
)
async def mono_ethereum_bep20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_ethereum_bep20()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO ETHEREUM CLASSIC"),
        (F.text.lower() == "mono to ethereum classic"),
    )
)
async def mono_ethereum_classic(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_ethereum_classic()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO RIPPLE"), (F.text.lower() == "mono to ripple")
    )
)
async def mono_ripple(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_ripple()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO LITECOIN"), (F.text.lower() == "mono to litecoin")
    )
)
async def mono_litecoin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_litecoin()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO MONERO"), (F.text.lower() == "mono to monero")
    )
)
async def mono_monero(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_monero()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO DOGECOIN"), (F.text.lower() == "mono to dogecoin")
    )
)
async def mono_dogecoin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_dogecoin()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO POLYGON"), (F.text.lower() == "mono to polygon")
    )
)
async def mono_polygon(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_polygon()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(filters.Command("MONO TO DASH"), (F.text.lower() == "mono to dash"))
)
async def mono_dash(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_dash()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(filters.Command("MONO TO ZCASH"), (F.text.lower() == "mono to zcash"))
)
async def mono_zcash(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_zcash()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO TETHER ERC20"),
        (F.text.lower() == "mono to tether erc20"),
    )
)
async def mono_tether_erc20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_tether_erc20()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO TETHER TRC20"),
        (F.text.lower() == "mono to tether trc20"),
    )
)
async def mono_tether_trc20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_tether_trc20()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO TETHER BEP20"),
        (F.text.lower() == "mono to tether bep20"),
    )
)
async def mono_tether_bep20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_tether_bep20()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO TETHER SOL"), (F.text.lower() == "mono to tether sol")
    )
)
async def mono_tether_sol(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_tether_sol()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO TETHER POLYGON"),
        (F.text.lower() == "mono to tether polygon"),
    )
)
async def mono_tether_polygon(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_tether_polygon()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO EURT ERC20"), (F.text.lower() == "mono to eurt erc20")
    )
)
async def mono_eurt_erc20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_eurt_erc20()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO USD COIN"), (F.text.lower() == "mono to usd coin")
    )
)
async def mono_usd_coin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_usd_coin()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO USD COIN TRC20"),
        (F.text.lower() == "mono to usd coin trc20"),
    )
)
async def mono_usd_coin_trc20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_usd_coin_trc20()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO USD COIN BEP20"),
        (F.text.lower() == "mono to usd coin bep20"),
    )
)
async def mono_usd_coin_bep20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_usd_coin_bep20()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO USD COIN SOL"),
        (F.text.lower() == "mono to usd coin sol"),
    )
)
async def mono_usd_coin_sol(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_usd_coin_sol()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO USD COIN POLYGON"),
        (F.text.lower() == "mono to usd coin polygon"),
    )
)
async def mono_usd_coin_polygon(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_usd_coin_polygon()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(filters.Command("MONO TO DAI"), (F.text.lower() == "mono to dai"))
)
async def mono_dai(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_dai()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO CARDANO"), (F.text.lower() == "mono to cardano")
    )
)
async def mono_cardano(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_cardano()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO STELLAR"), (F.text.lower() == "mono to stellar")
    )
)
async def mono_stellar(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_stellar()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(filters.Command("MONO TO TRON"), (F.text.lower() == "mono to tron"))
)
async def mono_tron(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_tron()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO BINANCE COIN"),
        (F.text.lower() == "mono to binance coin"),
    )
)
async def mono_binance_coin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_binance_coin()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO BINANCE COIN BEP20"),
        (F.text.lower() == "mono to binance coin bep20"),
    )
)
async def mono_binance_coin_bep2(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_binance_coin_bep2()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO COSMOS"), (F.text.lower() == "mono to cosmos")
    )
)
async def mono_cosmos(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_cosmos()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO POLKADOT"), (F.text.lower() == "mono to polkadot")
    )
)
async def mono_polkadot(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_polkadot()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO SOLANA"), (F.text.lower() == "mono to solana")
    )
)
async def mono_solana(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_solana()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("MONO TO PANCAKESWAP"),
        (F.text.lower() == "mono to pancakeswap"),
    )
)
async def mono_pancakeswap(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    mono_to_crypto.get_pancakeswap()
    with open(
        f"cryptocurrencies/data/{mono_to_crypto.file_name}.json", "r", encoding="utf-8"
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


# Privat To Crypto


@dp.message(
    filters.or_f(filters.Command("Privat Bank"), (F.text.lower() == "privat bank"))
)
async def base__privat_to_crypto(message: types.Message) -> None:
    await message.answer(
        "Оберіть крипту яка вас цікавить...", reply_markup=reply.privat_to_crypto
    )


privat_to_crypto = managers.PrivatToCrypto()


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO BITCOIN"), (F.text.lower() == "privat to bitcoin")
    )
)
async def privat_bitcoin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_bitcoin()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO BITCOIN BEP20"),
        (F.text.lower() == "privat to bitcoin bep20"),
    )
)
async def privat_bitcoin_bep20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_bitcoin_bep20()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO BITCOIN CASH"),
        (F.text.lower() == "privat to bitcoin cash"),
    )
)
async def privat_bitcoin_cash(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_bitcoin_cash()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO ETHEREUM"), (F.text.lower() == "privat to ethereum")
    )
)
async def privat_ethereum(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_ethereum()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO ETHEREUM BEP20"),
        (F.text.lower() == "privat to ethereum bep20"),
    )
)
async def privat_ethereum_bep20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_ethereum_bep20()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO ETHEREUM CLASSIC"),
        (F.text.lower() == "privat to ethereum classic"),
    )
)
async def privat_ethereum_classic(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_ethereum_classic()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO RIPPLE"), (F.text.lower() == "privat to ripple")
    )
)
async def privat_ripple(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_ripple()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO LITECOIN"), (F.text.lower() == "privat to litecoin")
    )
)
async def privat_litecoin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_litecoin()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO MONERO"), (F.text.lower() == "privat to monero")
    )
)
async def privat_monero(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_monero()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO DOGECOIN"), (F.text.lower() == "privat to dogecoin")
    )
)
async def privat_dogecoin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_dogecoin()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO POLYGON"), (F.text.lower() == "privat to polygon")
    )
)
async def privat_polygon(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_polygon()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO DASH"), (F.text.lower() == "privat to dash")
    )
)
async def privat_dash(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_dash()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO ZCASH"), (F.text.lower() == "privat to zcash")
    )
)
async def privat_zcash(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_zcash()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO TETHER ERC20"),
        (F.text.lower() == "privat to tether erc20"),
    )
)
async def privat_tether_erc20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_tether_erc20()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO TETHER TRC20"),
        (F.text.lower() == "privat to tether trc20"),
    )
)
async def privat_tether_trc20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_tether_trc20()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO TETHER BEP20"),
        (F.text.lower() == "privat to tether bep20"),
    )
)
async def privat_tether_bep20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_tether_bep20()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO TETHER SOL"),
        (F.text.lower() == "privat to tether sol"),
    )
)
async def privat_tether_sol(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_tether_sol()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO TETHER POLYGON"),
        (F.text.lower() == "privat to tether polygon"),
    )
)
async def privat_tether_polygon(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_tether_polygon()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO EURT ERC20"),
        (F.text.lower() == "privat to eurt erc20"),
    )
)
async def privat_eurt_erc20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_eurt_erc20()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO USD COIN"), (F.text.lower() == "privat to usd coin")
    )
)
async def privat_usd_coin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_usd_coin()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO USD COIN TRC20"),
        (F.text.lower() == "privat to usd coin trc20"),
    )
)
async def privat_usd_coin_trc20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_usd_coin_trc20()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO USD COIN BEP20"),
        (F.text.lower() == "privat to usd coin bep20"),
    )
)
async def privat_usd_coin_bep20(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_usd_coin_bep20()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO USD COIN SOL"),
        (F.text.lower() == "privat to usd coin sol"),
    )
)
async def privat_usd_coin_sol(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_usd_coin_sol()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO USD COIN POLYGON"),
        (F.text.lower() == "privat to usd coin polygon"),
    )
)
async def privat_usd_coin_polygon(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_usd_coin_polygon()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(filters.Command("PRIVAT TO DAI"), (F.text.lower() == "privat to dai"))
)
async def privat_dai(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_dai()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO CARDANO"), (F.text.lower() == "privat to cardano")
    )
)
async def privat_cardano(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_cardano()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO STELLAR"), (F.text.lower() == "privat to stellar")
    )
)
async def privat_stellar(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_stellar()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO TRON"), (F.text.lower() == "privat to tron")
    )
)
async def privat_tron(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_tron()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO BINANCE COIN"),
        (F.text.lower() == "privat to binance coin"),
    )
)
async def privat_binance_coin(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_binance_coin()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO BINANCE COIN BEP20"),
        (F.text.lower() == "privat to binance coin bep20"),
    )
)
async def privat_binance_coin_bep2(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_binance_coin_bep2()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO COSMOS"), (F.text.lower() == "privat to cosmos")
    )
)
async def privat_cosmos(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_cosmos()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO POLKADOT"), (F.text.lower() == "privat to polkadot")
    )
)
async def privat_polkadot(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_polkadot()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO SOLANA"), (F.text.lower() == "privat to solana")
    )
)
async def privat_solana(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_solana()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


@dp.message(
    filters.or_f(
        filters.Command("PRIVAT TO PANCAKESWAP"),
        (F.text.lower() == "privat to pancakeswap"),
    )
)
async def privat_pancakeswap(message: types.Message) -> None:
    await message.answer(mng.start_answer)
    privat_to_crypto.get_pancakeswap()
    with open(
        f"cryptocurrencies/data/{privat_to_crypto.file_name}.json",
        "r",
        encoding="utf-8",
    ) as file:
        data = json.load(file)
    for _, value in data.items():
        _text = mng.get_text(value)
        try:
            await message.answer(text=_text)
        except exceptions.TelegramRetryAfter:
            continue
    await message.answer(mng.final_answer)


async def main() -> None:
    await bot.set_my_commands(
        commands=[types.BotCommand(command="/start", description="Menu")],
        scope=types.BotCommandScopeAllPrivateChats(),
    )
    await dp.start_polling(bot)


asyncio.run(main())
