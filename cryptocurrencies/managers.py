import json
import requests
from bs4 import BeautifulSoup
from typing import Dict

try:
    import config
except ModuleNotFoundError:
    from . import config


class ScrapingCryptoCurrency:
    @classmethod
    def session(cls) -> requests.sessions.Session:
        return requests.Session()

    _domain = config.DOMAIN
    _headers = config.HEADERS
    _mono_to_crypto = config.MONO_TO_CRYPTO
    _privat_to_crypto = config.PRIVAT_TO_CRYPTO
    _start_message = config.START_MESSAGE
    _choice_message = config.CHOICE_MESSAGE
    _start_answer = config.START_ANSWER
    _answer = config.ANSWER
    _final_answer = config.FINAL_ANSWER
    _path = config.PATH

    @property
    def start_message(self) -> str:
        return self._start_message

    @start_message.setter
    def start_message(self, new_message: str):
        self._start_message = new_message

    @property
    def choice_message(self) -> str:
        return self._choice_message

    @choice_message.setter
    def choice_message(self, new_message: str):
        self._choice_message = new_message

    @property
    def start_answer(self) -> str:
        return self._start_answer

    @start_answer.setter
    def start_answer(self, new_start_answer: str):
        self._start_answer = new_start_answer

    @property
    def answer(self) -> str:
        return self._answer

    @answer.setter
    def answer(self, new_answer: str):
        self._answer = new_answer

    @property
    def final_answer(self) -> str:
        return self._final_answer

    @final_answer.setter
    def final_answer(self, new_final_answer: str):
        self._final_answer = new_final_answer

    @property
    def mono_to_crypto(self) -> Dict:
        return self._mono_to_crypto

    @property
    def privat_to_crypto(self) -> Dict:
        return self._privat_to_crypto

    @property
    def domain(self) -> str:
        return self._domain

    @property
    def headers(self) -> Dict:
        return self._headers

    @property
    def uri(self) -> str:
        return self._uri

    @uri.setter
    def uri(self, new_uri: str):
        self._uri = new_uri

    @property
    def file_name(self) -> str:
        return self._file_name

    @file_name.setter
    def file_name(self, new_file_name: str):
        self._file_name = new_file_name

    @property
    def path(self) -> str:
        return self._path

    @path.setter
    def path(self, new_path: str):
        self._path = new_path

    def sync_request(
        self,
        method: str,
        uri: str,
        headers=None,
        data=None,
    ) -> Dict:
        session = self.session()
        if method == "GET":
            response = session.get(uri, headers=headers)
        if method == "POST":
            response = session.post(uri, headers=headers, data=data)
        try:
            code = response.status_code
            response.raise_for_status()
            content = response.text
            return {"content": content}
        except requests.exceptions.HTTPError as exc:
            error_response = {"code": code, "detail": str(exc)}
            return error_response

    def get_content(self) -> None:
        _ = self.sync_request(method="GET", uri=self.uri, headers=self.headers)
        content = _.get("content")
        soul = BeautifulSoup(content, "lxml")
        items = soul.find(class_="wrapper").find(class_="m-rate").find_all("tr")
        i = 0
        data = {}
        for _ in items:
            rate = _.find(class_="fs")
            if rate is not None:
                is_verifying = _.find(class_="bj").find(class_="verifying")
                is_cardverify = _.find(class_="bj").find(class_="cardverify")
                is_reg = _.find(class_="bj").find(class_="reg")
                is_percent = _.find(class_="bj").find(class_="percent")
                if (
                    is_verifying is None
                    and is_cardverify is None
                    and is_reg is None
                    and is_percent is None
                ):
                    i += 1
                    href = (
                        _.find(
                            class_=[
                                "pa labpad1",
                                "pa labpad2",
                                "pa labpad3",
                                "pa labpad3",
                                "pa labpad4",
                                "pa labpad5",
                                "pa",
                            ]
                        )
                        .find("a")
                        .get("href")
                    )
                    reserve = _.find(class_="ar arp").text
                    data[f"{i}"] = {
                        "rate": rate.text,
                        "reserve": reserve,
                        "href": href,
                    }
        self.file_name = self.uri.split("/")[-1].split(".")[0]
        with open(f"{self.path}{self.file_name}.json", "w", encoding="utf-8") as file:
            json.dump(data, file, indent=4, ensure_ascii=False)

    def get_text(self, value: Dict) -> str:
        _text = self.answer.format(
            str(value.get("rate")), str(value.get("reserve")), str(value.get("href"))
        )
        return _text

    def read_text(self) -> Dict:
        with open(f"{self.path}{self.file_name}.json", "r", encoding="utf-8") as file:
            data = json.load(file)
        return data


class MonoToCrypto(ScrapingCryptoCurrency):
    def get_bitcoin(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_bitcoin")
        self.get_content()

    def get_bitcoin_bep20(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_bitcoin_bep20")
        self.get_content()

    def get_bitcoin_cash(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_bitcoin_cash")
        self.get_content()

    def get_ethereum(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_ethereum")
        self.get_content()

    def get_ethereum_bep20(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_ethereum_bep20")
        self.get_content()

    def get_ethereum_classic(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_ethereum_classic")
        self.get_content()

    def get_ripple(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_ripple")
        self.get_content()

    def get_litecoin(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_litecoin")
        self.get_content()

    def get_monero(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_monero")
        self.get_content()

    def get_dogecoin(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_dogecoin")
        self.get_content()

    def get_polygon(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_polygon")
        self.get_content()

    def get_dash(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_dash")
        self.get_content()

    def get_zcash(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_zcash")
        self.get_content()

    def get_tether_erc20(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_tether_erc20")
        self.get_content()

    def get_tether_trc20(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_tether_trc20")
        self.get_content()

    def get_tether_bep20(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_tether_bep20")
        self.get_content()

    def get_tether_sol(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_tether_sol")
        self.get_content()

    def get_tether_polygon(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_tether_polygon")
        self.get_content()

    def get_eurt_erc20(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_eurt_erc20")
        self.get_content()

    def get_usd_coin(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_usd_coin")
        self.get_content()

    def get_usd_coin_trc20(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_usd_coin_trc20")
        self.get_content()

    def get_usd_coin_bep20(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_usd_coin_bep20")
        self.get_content()

    def get_usd_coin_sol(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_usd_coin_sol")
        self.get_content()

    def get_usd_coin_polygon(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_usd_coin_polygon")
        self.get_content()

    def get_dai(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_dai")
        self.get_content()

    def get_cardano(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_cardano")
        self.get_content()

    def get_stellar(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_stellar")
        self.get_content()

    def get_tron(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_tron")
        self.get_content()

    def get_binance_coin(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_binance_coin")
        self.get_content()

    def get_binance_coin_bep2(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_binance_coin_bep2")
        self.get_content()

    def get_cosmos(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_cosmos")
        self.get_content()

    def get_polkadot(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_polkadot")
        self.get_content()

    def get_solana(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_solana")
        self.get_content()

    def get_pancakeswap(self) -> None:
        self.uri = self.domain + self.mono_to_crypto.get("mono_to_pancakeswap")
        self.get_content()


class PrivatToCrypto(ScrapingCryptoCurrency):
    def get_bitcoin(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_bitcoin")
        self.get_content()

    def get_bitcoin_bep20(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_bitcoin_bep20")
        self.get_content()

    def get_bitcoin_cash(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_bitcoin_cash")
        self.get_content()

    def get_ethereum(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_ethereum")
        self.get_content()

    def get_ethereum_bep20(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_ethereum_bep20")
        self.get_content()

    def get_ethereum_classic(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_ethereum_classic")
        self.get_content()

    def get_ripple(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_ripple")
        self.get_content()

    def get_litecoin(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_litecoin")
        self.get_content()

    def get_monero(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_monero")
        self.get_content()

    def get_dogecoin(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_dogecoin")
        self.get_content()

    def get_polygon(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_polygon")
        self.get_content()

    def get_dash(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_dash")
        self.get_content()

    def get_zcash(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_zcash")
        self.get_content()

    def get_tether_erc20(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_tether_erc20")
        self.get_content()

    def get_tether_trc20(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_tether_trc20")
        self.get_content()

    def get_tether_bep20(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_tether_bep20")
        self.get_content()

    def get_tether_sol(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_tether_sol")
        self.get_content()

    def get_tether_polygon(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_tether_polygon")
        self.get_content()

    def get_eurt_erc20(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_eurt_erc20")
        self.get_content()

    def get_usd_coin(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_usd_coin")
        self.get_content()

    def get_usd_coin_trc20(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_usd_coin_trc20")
        self.get_content()

    def get_usd_coin_bep20(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_usd_coin_bep20")
        self.get_content()

    def get_usd_coin_sol(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_usd_coin_sol")
        self.get_content()

    def get_usd_coin_polygon(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_usd_coin_polygon")
        self.get_content()

    def get_dai(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_dai")
        self.get_content()

    def get_cardano(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_cardano")
        self.get_content()

    def get_stellar(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_stellar")
        self.get_content()

    def get_tron(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_tron")
        self.get_content()

    def get_binance_coin(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_binance_coin")
        self.get_content()

    def get_binance_coin_bep2(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get(
            "privat_to_binance_coin_bep2"
        )
        self.get_content()

    def get_cosmos(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_cosmos")
        self.get_content()

    def get_polkadot(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_polkadot")
        self.get_content()

    def get_solana(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_solana")
        self.get_content()

    def get_pancakeswap(self) -> None:
        self.uri = self.domain + self.privat_to_crypto.get("privat_to_pancakeswap")
        self.get_content()
