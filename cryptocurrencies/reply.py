from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

start_kb = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="Mono Bank"),
            KeyboardButton(text="Privat Bank"),
        ],
    ],
    resize_keyboard=True,
)

mono_to_crypto = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="MONO TO BITCOIN"),
            KeyboardButton(text="MONO TO ETHEREUM"),
            KeyboardButton(text="MONO TO LITECOIN"),
            KeyboardButton(text="MONO TO RIPPLE"),
        ],
        [
            KeyboardButton(text="MONO TO SOLANA"),
            KeyboardButton(text="MONO TO BITCOIN BEP20"),
            KeyboardButton(text="MONO TO BITCOIN CASH"),
            KeyboardButton(text="MONO TO ETHEREUM BEP20"),
        ],
        [
            KeyboardButton(text="MONO TO ETHEREUM CLASSIC"),
            KeyboardButton(text="MONO TO MONERO"),
            KeyboardButton(text="MONO TO POLYGON"),
            KeyboardButton(text="MONO TO DASH"),
        ],
        [
            KeyboardButton(text="MONO TO ZCASH"),
            KeyboardButton(text="MONO TO TETHER ERC20"),
            KeyboardButton(text="MONO TO TETHER TRC20"),
            KeyboardButton(text="MONO TO TETHER BEP20"),
        ],
        [
            KeyboardButton(text="MONO TO TETHER SOL"),
            KeyboardButton(text="MONO TO TETHER POLYGON"),
            KeyboardButton(text="MONO TO EURT ERC20"),
            KeyboardButton(text="MONO TO USD COIN"),
        ],
        [
            KeyboardButton(text="MONO TO USD COIN TRC20"),
            KeyboardButton(text="MONO TO USD COIN BEP20"),
            KeyboardButton(text="MONO TO USD COIN SOL"),
            KeyboardButton(text="MONO TO USD COIN POLYGON"),
        ],
        [
            KeyboardButton(text="MONO TO DAI"),
            KeyboardButton(text="MONO TO CARDANO"),
            KeyboardButton(text="MONO TO STELLAR"),
            KeyboardButton(text="MONO TO TRON"),
        ],
        [
            KeyboardButton(text="MONO TO BINANCE COIN"),
            KeyboardButton(text="MONO TO DOGECOIN"),
            KeyboardButton(text="MONO TO BINANCE COIN BEP20"),
            KeyboardButton(text="MONO TO COSMOS"),
        ],
        [
            KeyboardButton(text="MONO TO POLKADOT"),
            KeyboardButton(text="MONO TO PANCAKESWAP"),
        ],
    ],
    resize_keyboard=True,
)

privat_to_crypto = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text="PRIVAT TO BITCOIN"),
            KeyboardButton(text="PRIVAT TO ETHEREUM"),
            KeyboardButton(text="PRIVAT TO LITECOIN"),
            KeyboardButton(text="PRIVAT TO RIPPLE"),
            KeyboardButton(text="PRIVAT TO SOLANA"),
        ],
        [
            KeyboardButton(text="PRIVAT TO BITCOIN BEP20"),
            KeyboardButton(text="PRIVAT TO BITCOIN CASH"),
            KeyboardButton(text="PRIVAT TO ETHEREUM BEP20"),
            KeyboardButton(text="PRIVAT TO ETHEREUM CLASSIC"),
            KeyboardButton(text="PRIVAT TO MONERO"),
        ],
        [
            KeyboardButton(text="PRIVAT TO POLYGON"),
            KeyboardButton(text="PRIVAT TO DASH"),
            KeyboardButton(text="PRIVAT TO ZCASH"),
            KeyboardButton(text="PRIVAT TO TETHER ERC20"),
            KeyboardButton(text="PRIVAT TO TETHER TRC20"),
        ],
        [
            KeyboardButton(text="PRIVAT TO TETHER BEP20"),
            KeyboardButton(text="PRIVAT TO TETHER SOL"),
            KeyboardButton(text="PRIVAT TO TETHER POLYGON"),
            KeyboardButton(text="PRIVAT TO EURT ERC20"),
            KeyboardButton(text="PRIVAT TO USD COIN"),
        ],
        [
            KeyboardButton(text="PRIVAT TO USD COIN TRC20"),
            KeyboardButton(text="PRIVAT TO USD COIN BEP20"),
            KeyboardButton(text="PRIVAT TO USD COIN SOL"),
            KeyboardButton(text="PRIVAT TO USD COIN POLYGON"),
            KeyboardButton(text="PRIVAT TO DAI"),
        ],
        [
            KeyboardButton(text="PRIVAT TO CARDANO"),
            KeyboardButton(text="PRIVAT TO STELLAR"),
            KeyboardButton(text="PRIVAT TO TRON"),
            KeyboardButton(text="PRIVAT TO BINANCE COIN"),
            KeyboardButton(text="PRIVAT TO DOGECOIN"),
        ],
        [
            KeyboardButton(text="PRIVAT TO BINANCE COIN BEP20"),
            KeyboardButton(text="PRIVAT TO COSMOS"),
            KeyboardButton(text="PRIVAT TO POLKADOT"),
            KeyboardButton(text="PRIVAT TO PANCAKESWAP"),
        ],
    ],
    resize_keyboard=True,
)
