HEADERS = {
    "Dnt": "1",
    "Referer": "https://www.bestchange.com/",
    "Sec-Ch-Ua": '"Not_A Brand";v="8", "Chromium";v="120", "Avast Secure Browser";v="120"',
    "Sec-Ch-Ua-Mobile": "?0",
    "Sec-Ch-Ua-Platform": "Windows",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 Avast/120.0.0.0",
}

DOMAIN = "https://www.bestchange.com"

START_MESSAGE = "Привіт! Оберіть ваш банк..."

CHOICE_MESSAGE = "Оберіть крипту яка вас цікавить..."

START_ANSWER = "Зачекайте..."

ANSWER = """
    КУРС: {0} \nРЕЗЕРВ: {1} \n{2}
"""

PATH = "cryptocurrencies/data/"

FINAL_ANSWER = "Ось найвигідніші курси для вас..."

MONO_TO_CRYPTO = {
    "mono_to_bitcoin": "/monobank-to-bitcoin.html",
    "mono_to_bitcoin_bep20": "/monobank-to-bitcoin-bep20.html",
    "mono_to_bitcoin_cash": "/monobank-to-bitcoin-cash.html",
    "mono_to_ethereum": "/monobank-to-ethereum.html",
    "mono_to_ethereum_bep20": "/monobank-to-ethereum-bep20.html",
    "mono_to_ethereum_classic": "/monobank-to-ethereum-classic.html",
    "mono_to_ripple": "/monobank-to-ripple.html",
    "mono_to_litecoin": "/monobank-to-litecoin.html",
    "mono_to_monero": "/monobank-to-monero.html",
    "mono_to_dogecoin": "/monobank-to-dogecoin.html",
    "mono_to_polygon": "/monobank-to-polygon.html",
    "mono_to_dash": "/monobank-to-dash.html",
    "mono_to_zcash": "/monobank-to-zcash.html",
    "mono_to_tether_erc20": "/monobank-to-tether-erc20.html",
    "mono_to_tether_trc20": "/monobank-to-tether-trc20.html",
    "mono_to_tether_bep20": "/monobank-to-tether-bep20.html",
    "mono_to_tether_sol": "/monobank-to-tether-sol.html",
    "mono_to_tether_polygon": "/monobank-to-tether-polygon.html",
    "mono_to_eurt_erc20": "/monobank-to-eurt-erc20.html",
    "mono_to_usd_coin": "/monobank-to-usd-coin.html",
    "mono_to_usd_coin_trc20": "/monobank-to-usd-coin-trc20.html",
    "mono_to_usd_coin_bep20": "/monobank-to-usd-coin-bep20.html",
    "mono_to_usd_coin_sol": "/monobank-to-usd-coin-sol.html",
    "mono_to_usd_coin_polygon": "/monobank-to-usd-coin-polygon.html",
    "mono_to_dai": "/monobank-to-dai.html",
    "mono_to_cardano": "/monobank-to-cardano.html",
    "mono_to_stellar": "/monobank-to-stellar.html",
    "mono_to_tron": "/monobank-to-tron.html",
    "mono_to_binance_coin": "/monobank-to-binance-coin.html",
    "mono_to_binance_coin_bep2": "/monobank-to-binance-coin-bep2.html",
    "mono_to_cosmos": "/monobank-to-cosmos.html",
    "mono_to_polkadot": "/monobank-to-polkadot.html",
    "mono_to_solana": "/monobank-to-solana.html",
    "mono_to_pancakeswap": "/monobank-to-pancakeswap.html",
}

PRIVAT_TO_CRYPTO = {
    "privat_to_bitcoin": "/privat24-uah-to-bitcoin.html",
    "privat_to_bitcoin_bep20": "/privat24-uah-to-bitcoin-bep20.html",
    "privat_to_bitcoin_cash": "/privat24-uah-to-bitcoin-cash.html",
    "privat_to_ethereum": "/privat24-uah-to-ethereum.html",
    "privat_to_ethereum_bep20": "/privat24-uah-to-ethereum-bep20.html",
    "privat_to_ethereum_classic": "/privat24-uah-to-ethereum-classic.html",
    "privat_to_ripple": "/privat24-uah-to-ripple.html",
    "privat_to_litecoin": "/privat24-uah-to-litecoin.html",
    "privat_to_monero": "/privat24-uah-to-monero.html",
    "privat_to_dogecoin": "/privat24-uah-to-dogecoin.html",
    "privat_to_polygon": "/privat24-uah-to-polygon.html",
    "privat_to_dash": "/privat24-uah-to-dash.html",
    "privat_to_zcash": "/privat24-uah-to-zcash.html",
    "privat_to_tether_erc20": "/privat24-uah-to-tether-erc20.html",
    "privat_to_tether_trc20": "/privat24-uah-to-tether-trc20.html",
    "privat_to_tether_bep20": "/privat24-uah-to-tether-bep20.html",
    "privat_to_tether_sol": "/privat24-uah-to-tether-sol.html",
    "privat_to_tether_polygon": "/privat24-uah-to-tether-polygon.html",
    "privat_to_eurt_erc20": "/privat24-uah-to-eurt-erc20.html",
    "privat_to_usd_coin": "/privat24-uah-to-usd-coin.html",
    "privat_to_usd_coin_trc20": "/privat24-uah-to-usd-coin-trc20.html",
    "privat_to_usd_coin_bep20": "/privat24-uah-to-usd-coin-bep20.html",
    "privat_to_usd_coin_sol": "/privat24-uah-to-usd-coin-sol.html",
    "privat_to_usd_coin_polygon": "/privat24-uah-to-usd-coin-polygon.html",
    "privat_to_dai": "/privat24-uah-to-dai.html",
    "privat_to_cardano": "/privat24-uah-to-cardano.html",
    "privat_to_stellar": "/privat24-uah-to-stellar.html",
    "privat_to_tron": "/privat24-uah-to-tron.html",
    "privat_to_binance_coin": "/privat24-uah-to-binance-coin.html",
    "privat_to_binance_coin_bep2": "/privat24-uah-to-binance-coin-bep2.html",
    "privat_to_cosmos": "/privat24-uah-to-cosmos.html",
    "privat_to_polkadot": "/privat24-uah-to-polkadot.html",
    "privat_to_solana": "/privat24-uah-to-solana.html",
    "privat_to_pancakeswap": "/privat24-uah-to-pancakeswap.html",
}
