from typing import Dict
from fastapi import APIRouter
from cryptocurrencies import managers


router = APIRouter(tags=["Privat to crypto"], prefix="/privat-to-crypto")
mng = managers.PrivatToCrypto()


@router.get("/bitcoin")
async def bitcoin() -> Dict:
    mng.get_bitcoin()
    payload = mng.read_text()
    return payload


@router.get("/bitcoin-bep20")
async def bitcoin_bep20() -> Dict:
    mng.get_bitcoin_bep20()
    payload = mng.read_text()
    return payload


@router.get("/bitcoin-cash")
async def bitcoin_cash() -> Dict:
    mng.get_bitcoin_cash()
    payload = mng.read_text()
    return payload


@router.get("/ethereum")
async def ethereum() -> Dict:
    mng.get_ethereum()
    payload = mng.read_text()
    return payload


@router.get("/ethereum-bep20")
async def ethereum_bep20() -> Dict:
    mng.get_ethereum_bep20()
    payload = mng.read_text()
    return payload


@router.get("/ethereum-classic")
async def ethereum_classic() -> Dict:
    mng.get_ethereum_classic()
    payload = mng.read_text()
    return payload


@router.get("/ripple")
async def ripple() -> Dict:
    mng.get_ripple()
    payload = mng.read_text()
    return payload


@router.get("/litecoin")
async def litecoin() -> Dict:
    mng.get_litecoin()
    payload = mng.read_text()
    return payload


@router.get("/monero")
async def monero() -> Dict:
    mng.get_monero()
    payload = mng.read_text()
    return payload


@router.get("/dogecoin")
async def dogecoin() -> Dict:
    mng.get_dogecoin()
    payload = mng.read_text()
    return payload


@router.get("/polygon")
async def polygon() -> Dict:
    mng.get_polygon()
    payload = mng.read_text()
    return payload


@router.get("/dash")
async def dash() -> Dict:
    mng.get_dash()
    payload = mng.read_text()
    return payload


@router.get("/zcash")
async def zcash() -> Dict:
    mng.get_zcash()
    payload = mng.read_text()
    return payload


@router.get("/tether-erc20")
async def tether_erc20() -> Dict:
    mng.get_tether_erc20()
    payload = mng.read_text()
    return payload


@router.get("/tether-trc20")
async def tether_trc20() -> Dict:
    mng.get_tether_trc20()
    payload = mng.read_text()
    return payload


@router.get("/tether-bep20")
async def tether_bep20() -> Dict:
    mng.get_tether_bep20()
    payload = mng.read_text()
    return payload


@router.get("/tether-sol")
async def tether_sol() -> Dict:
    mng.get_tether_sol()
    payload = mng.read_text()
    return payload


@router.get("/tether-polygon")
async def tether_polygon() -> Dict:
    mng.get_tether_polygon()
    payload = mng.read_text()
    return payload


@router.get("/eurt-erc20")
async def eurt_erc20() -> Dict:
    mng.get_eurt_erc20()
    payload = mng.read_text()
    return payload


@router.get("/usd-coin")
async def usd_coin() -> Dict:
    mng.get_usd_coin()
    payload = mng.read_text()
    return payload


@router.get("/usd-coin-trc20")
async def usd_coin_trc20() -> Dict:
    mng.get_usd_coin_trc20()
    payload = mng.read_text()
    return payload


@router.get("/usd-coin-bep20")
async def usd_coin_bep20() -> Dict:
    mng.get_usd_coin_bep20()
    payload = mng.read_text()
    return payload


@router.get("/usd-coin-sol")
async def usd_coin_sol() -> Dict:
    mng.get_usd_coin_sol()
    payload = mng.read_text()
    return payload


@router.get("/usd-coin-polygon")
async def usd_coin_polygon() -> Dict:
    mng.get_usd_coin_polygon()
    payload = mng.read_text()
    return payload


@router.get("/dai")
async def dai() -> Dict:
    mng.get_dai()
    payload = mng.read_text()
    return payload


@router.get("/cardano")
async def cardano() -> Dict:
    mng.get_cardano()
    payload = mng.read_text()
    return payload


@router.get("/stellar")
async def stellar() -> Dict:
    mng.get_stellar()
    payload = mng.read_text()
    return payload


@router.get("/tron")
async def tron() -> Dict:
    mng.get_tron()
    payload = mng.read_text()
    return payload


@router.get("/binance-coin")
async def binance_coin() -> Dict:
    mng.get_binance_coin()
    payload = mng.read_text()
    return payload


@router.get("/binance-coin-bep2")
async def binance_coin_bep2() -> Dict:
    mng.get_binance_coin_bep2()
    payload = mng.read_text()
    return payload


@router.get("/cosmos")
async def cosmos() -> Dict:
    mng.get_cosmos()
    payload = mng.read_text()
    return payload


@router.get("/polkadot")
async def polkadot() -> Dict:
    mng.get_polkadot()
    payload = mng.read_text()
    return payload


@router.get("/solana")
async def solana() -> Dict:
    mng.get_solana()
    payload = mng.read_text()
    return payload


@router.get("/pancakeswap")
async def pancakeswap() -> Dict:
    mng.get_pancakeswap()
    payload = mng.read_text()
    return payload
