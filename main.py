from fastapi import FastAPI
from routers.mono_to_crypto_router import router as mono_to_crypto_router
from routers.privat_to_crypto_router import router as privat_to_crypto_router


app = FastAPI(title="Cryptocurrencies Agregator")


app.include_router(mono_to_crypto_router)
app.include_router(privat_to_crypto_router)
